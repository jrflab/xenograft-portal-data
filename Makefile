PORTAL_HOME?=/Users/debruiji/git/cbioportal-mini
MYSQL_CONNECT?=mysql --user=cbio --password=cbio xenograft
JAVA_BIN?=java
PYTHON_BIN?=python2.7
CORE_JAR?=$(shell ls $(PORTAL_HOME)/core/target/core-*-SNAPSHOT.jar)

seed/cbioportal-seed.sql.gz: 
	mkdir -p seed
	wget http://cbio.mskcc.org/cancergenomics/public-portal/downloads/cbioportal-seed.sql.gz && mv cbioportal-seed.sql.gz $@

seed: seed/cbioportal-seed.sql.gz
	$(MYSQL_CONNECT) < $<

clean:
	for t in copy_number_seg copy_number_seg_file sample mutation patient patient_list patient_list_list cancer_study cna_event genetic_alteration genetic_profile genetic_profile_samples mutation_count mutation_event sample_profile clinical_patient clinical_sample clinical_attribute clinical_event sample_cna_event text_cache; do echo truncate table $$t';'; done | $(MYSQL_CONNECT)

import:
	$(JAVA_BIN) -Dspring.profiles.active=dbcp -cp $(CORE_JAR) org.mskcc.cbio.portal.scripts.ImportCancerStudy ~/bb/xenograft-portal-data/meta_study.txt && \
	$(JAVA_BIN) -Dspring.profiles.active=dbcp -cp $(CORE_JAR) org.mskcc.cbio.portal.scripts.ImportClinicalData data_all_clinical.txt esr1_pdx_2013 && \
	$(PYTHON_BIN) $(PORTAL_HOME)/core/src/main/scripts/cbioportalImporter.py --jvm-args "-Dspring.profiles.active=dbcp -cp $(CORE_JAR)" --command import-study-data --meta-filename meta_mutation.txt --data-filename data_mutations_all.maf && \
	$(PYTHON_BIN) $(PORTAL_HOME)/core/src/main/scripts/cbioportalImporter.py --jvm-args "-Dspring.profiles.active=dbcp -cp $(CORE_JAR)" --command import-study-data --meta-filename meta_rnaseq.txt --data-filename whim.4.rsem.uqnorm.txt && \
	$(JAVA_BIN) -Dspring.profiles.active=dbcp -cp $(CORE_JAR) org.mskcc.cbio.portal.scripts.ImportCopyNumberSegmentData --data chris-miller-cn/allsamples.cbs --meta chris-miller-cn/meta.cbs.txt && \
	$(PYTHON_BIN) $(PORTAL_HOME)/core/src/main/scripts/cbioportalImporter.py --jvm-args "-Dspring.profiles.active=dbcp -cp $(CORE_JAR)" --command import-study-data --meta-filename meta.CNA.txt --data-filename data_cnv_tumor_xeno.txt && \
	$(PYTHON_BIN) $(PORTAL_HOME)/core/src/main/scripts/cbioportalImporter.py --jvm-args "-Dspring.profiles.active=dbcp -cp $(CORE_JAR)" --command import-case-list --meta-filename case_lists/cases_all.txt
