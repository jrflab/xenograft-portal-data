#!/bin/bash

export INPUT_VCF=$1
export OUTPUT_MAF=$2
export TUMOR_ID=$3
export NORMAL_ID=$4
# dashi-dev config
# annotator tool
#18Aug2015   v22
export MAF2MAF=/opt/common/CentOS_6/vcf2maf/v1.6.1/maf2maf.pl
export VCF2MAF=/opt/common/CentOS_6/vcf2maf/v1.6.1/vcf2maf.pl
export PERL_BIN=/opt/common/CentOS_6-dev/perl/perl-5.22.0/bin/perl
#export PERL_LIB=PERL5LIB=/opt/common/CentOS_6-dev/perl/perl-5.22.0/lib/5.22.0:/opt/common/CentOS_6-dev/perl/perl-5.22.0/lib/perl5:/opt/common/CentOS_6-dev/perl/perl-5.22.0/lib/perl5/x86_64-linux
export PERL5LIB=/opt/common/CentOS_6-dev/perl/perl-5.22.0/lib/5.22.0:/opt/common/CentOS_6-dev/perl/perl-5.22.0/lib/perl5:/opt/common/CentOS_6-dev/perl/perl-5.22.0/lib/perl5/x86_64-linux
export VEP_PATH=/opt/common/CentOS_6/vep/v81
export VEP_DATA=/opt/common/CentOS_6/vep/v81
export REF_FASTA=/ssd-data/cmo/opt/vep/v79/homo_sapiens/79_GRCh37/Homo_sapiens.GRCh37.75.dna.primary_assembly.fa
#export REF_FASTA=/home/debruiji/bb/xenograft-portal-data/human_g1k_v37.fasta
export EXCLUDE_COLS=oncotator
export INCLUDE_COLS=Hugo_Symbol,Entrez_Gene_Id,Center,NCBI_Build,Chromosome,Start_Position,End_Position,Strand,Variant_Classification,Variant_Type,Reference_Allele,Tumor_Seq_Allele1,Tumor_Seq_Allele2,dbSNP_RS,dbSNP_Val_Status,Tumor_Sample_Barcode,Matched_Norm_Sample_Barcode,Match_Norm_Seq_Allele1,Match_Norm_Seq_Allele2,Tumor_Validation_Allele1,Tumor_Validation_Allele2,Match_Norm_Validation_Allele1,Match_Norm_Validation_Allele2,Verification_Status,Validation_Status,Mutation_Status,Sequencing_Phase,Sequence_Source,Validation_Method,Score,BAM_File,Sequencer,Tumor_Sample_UUID,Matched_Norm_Sample_UUID,t_ref_count,t_alt_count,n_ref_count,n_alt_count,cDNA_Change,Amino_Acid_Change,Transcript
export MODE=regular
export INTERMEDIATE_MAF=
export VEP_FORKS=8
export PATH=$PATH:/opt/common/CentOS_6-dev/perl/perl-5.22.0/bin/:/ssd-data/cmo/bin
export CLUSTER_NODE=
export CLUSTER_SP=
export CUSTOM_ENST=/opt/common/CentOS_6/vcf2maf/v1.6.1/data/isoform_overrides_at_mskcc

#execute maf2maf perl script
$PERL_BIN $VCF2MAF --vep-path $VEP_PATH --vep-data $VEP_DATA --ref-fasta $REF_FASTA --input-vcf $INPUT_VCF --output-maf $OUTPUT_MAF --vep-forks $VEP_FORKS --custom-enst $CUSTOM_ENST --tumor-id $TUMOR_ID --normal-id $NORMAL_ID
